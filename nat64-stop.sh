#!/bin/bash

if [[ $(lsmod |grep nat64 |wc -l) -gt 0 ]];then
	modprobe -r nf_nat64
	if [[ $? -ne 0 ]];then
		echo "Can't unload module nf_nat64!"
		exit 1
	else
		echo "Unload module nf_nat64 successfully!"
		exit 0
	fi
else
	echo "Module nf_nat64 doesn't load!"
	exit 0
fi
