#!/bin/bash

TOP_DIR=$(cd $(dirname "$0") && pwd)
if [[ -e /etc/nat64.conf ]];then
	source /etc/nat64.conf
else
	echo "Can't find /etc/nat64.conf"
	exit 1
fi

ARGS=

echo "**************"
echo "nf_nat64 setup"
echo "**************"

myrun()
{
	echo "$1"
	$1
}
get_cur_iface()
{
    ip route show | awk '/default/{print $5}'
}
get_iface_ip4()
{
    ip addr show dev $1 | awk '/inet /{print $2}' | cut -d'/' -f1
}

# Must have a global scope ipv6 address
if [[ $(ip -6 addr show scope global | grep inet6 | wc -l) == 0 ]]; then
	echo "ERROR : No global scope ipv6 address!"
	exit 1
fi

# Check arguments
if [[ -z "$IPV4_ADDR" ]];then
	echo "WARN : No ipv4 pool found!"
	IPV4_ADDR=$(get_iface_ip4 $(get_cur_iface))
	if [[ -z "$IPV4_ADDR" ]];then
		echo "ERROR : Can't get ipv4 addr from default route!"
		exit 1
	fi
	IPV4_MASK=32
	echo "WARN : Will use ${IPV4_ADDR}/${IPV4_MASK}"
fi
ARGS="nat64_ipv4_addr=$IPV4_ADDR "

if [ -z "$IPV4_MASK" ] || [ $IPV4_MASK -gt 32 ]; then
	echo "ERROR : IPV4_MASK value $IPV4_MASK is invalid!"
	exit 1	
fi
ARGS=${ARGS}"nat64_ipv4_mask=$IPV4_MASK "

# if [ -z "$PREFIX_ADDR" ] || [ -z "$PREFIX_LEN" ] || [ $PREFIX_LEN -gt 96 ];then
	# echo "ERROR : Invalid PREFIX_ADDR or PREFIX_MASK"
	# exit 1
# fi
# ARGS=${ARGS}"nat64_prefix_addr=$PREFIX_ADDR nat64_prefix_len=$PREFIX_LEN "

if [ $PREFIX_LEN -le 96 ];then
    ARGS=${ARGS}"nat64_prefix_len=$PREFIX_LEN "
fi

if [ ! -z "$ETH_NAME" ];then
    echo "Virtual Netdevice Name is $ETH_NAME"
    ARGS=${ARGS}"ethname=$ETH_NAME "
fi

echo
echo "Info: Using ${IPV4_ADDR}/${IPV4_MASK} as the NAT64 IPv4 address."
# echo "Info: Using ${PREFIX_ADDR}/${PREFIX_LEN} as the NAT64 Prefix."
echo

#set -ex

# Load the nf_nat64 module
myrun "modprobe -r nf_nat64"
#modprobe nf_nat64 nat64_ipv4_addr=$IPV4_ADDR nat64_prefix_addr=$PREFIX_ADDR nat64_prefix_len=$PREFIX_LEN nat64_ipv4_mask=$IPV4_MASK manage_eth=$MANAGE_ETH
myrun "modprobe nf_nat64 $ARGS"

# Enable the nat64 network interface
myrun "ifconfig $ETH_NAME mtu 1500 up"

# Install the route to the nat64 prefix
# myrun "ip -6 route add ${PREFIX_ADDR}/${PREFIX_LEN} dev nat64"
myrun "ip route add ${IPV4_ADDR}/${IPV4_MASK} dev $ETH_NAME"
for ip6addr in ${IPV6_ADDR[@]};do
	myrun "ip -6 route add $ip6addr dev $ETH_NAME"
done

# Enable ipv6 and ipv4 forwarding
myrun "sysctl -w net.ipv4.conf.all.forwarding=1"
myrun "sysctl -w net.ipv6.conf.all.forwarding=1"
exit 0
